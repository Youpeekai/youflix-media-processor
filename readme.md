# Youflix Media Processor
## What is it ?
Youflix Media Processor aims at providing a free, open and reliable post-processing tool for downloaded movies and TV show episodes. 

The application is a **[Java 12](https://openjdk.java.net/projects/jdk/12/spec/)** (OpenJDK), **[Spring Boot](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/htmlsingle/)** project, compiled with **[Maven](https://maven.apache.org/)**.

## Modules
### Renaming module
Renaming is based on data provided by [**TheMovieDataBase** API](https://developers.themoviedb.org/3). 
For a given file name, the module will try to find the corresponding media by searching TMDB API.
It will then use the collected data to rename and move the file according to the configured template.  
The general philosophy of this module is *sureness*. It will not try to find a match on a file at all costs.
If the results are ambiguous, the affected file will be moved to error directory with an associated log.

### Subtitles Module
**Not implemented yet**  
This module will download subtitles matching processed movies & episodes.
It will use open API like [OpenSubtitles](https://www.opensubtitles.org) or [SubDB](http://thesubdb.com/api/).
### MKV merge with MKVToolnix
**Not implemented yet**  
Once the files are renamed and the subtitles are downloaded, [MKVToolnix](https://mkvtoolnix.download/) will be called to merge video file & subtitles files into one MKV container.


## Quick Start
### Build
The project is build with **maven**.
To generate the executable JAR, compile with maven goal `package`.
### Execution
Once the JAR is built, execute it with standard java CLI :
`java -jar youflix-medias-processor-0.0.1-SNAPSHOT.jar [parameters]`.
#### Parameters
All parameters are declared and described in **src/main/ressources/application.properties**.
You can override them all (as all [Spring Boot parameters](https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html)) on run via CLI options.
- Either by providing parameters one by one :  
`java -jar youflix-medias-processor-0.0.1-SNAPSHOT.jar --path.input=c:/input --path.output=c:/output --path.error=c:/error ...`
- Or by providing an external properties file :  
`java -jar youflix-medias-processor-0.0.1-SNAPSHOT.jar --spring.config.additional-location=file:cmd.properties`

Mandatory parameters to set are :
- `path.input` : input directory, must exists, be readable & writable
- `path.output` : output directory, must exists, be readable & writable
- `path.error` : error directory for unprocessable files, must exists, be readable & writable
- `api.tmdb.key` : API Key, mandatory to query TMDB API. See https://developers.themoviedb.org/3/getting-started/introduction



