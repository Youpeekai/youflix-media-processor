package you.project.youflixmediasprocessor.tmdb.dto;

public class MovieCollectionDTO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
