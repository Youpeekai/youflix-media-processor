package you.project.youflixmediasprocessor.tmdb.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ShowDTO extends ResultDTO {

    private int id;

    @JsonProperty("original_name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
