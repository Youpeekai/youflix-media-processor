package you.project.youflixmediasprocessor.tmdb.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ShowsDTO extends ResultDTO {

    @JsonProperty("results")
    List<ShowDTO> shows;

    public List<ShowDTO> getShows() {
        return shows;
    }

    public void setShows(List<ShowDTO> shows) {
        this.shows = shows;
    }

    @JsonIgnore
    public int size(){
        if(shows == null){
            return 0;
        }
        return shows.size();
    }
}
