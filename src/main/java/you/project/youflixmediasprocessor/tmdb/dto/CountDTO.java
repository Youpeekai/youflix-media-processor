package you.project.youflixmediasprocessor.tmdb.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CountDTO {

    @JsonProperty("total_results")
    private Integer results;
    @JsonProperty("total_pages")
    private Integer pages;

    public Integer getResults() {
        return results;
    }

    public void setResults(Integer results) {
        this.results = results;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }
}
