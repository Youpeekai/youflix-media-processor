package you.project.youflixmediasprocessor.tmdb.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class MovieDTO extends ResultDTO {

    private int id;

    @JsonProperty("original_title")
    String title;
    @JsonProperty("belongs_to_collection")
    MovieCollectionDTO collection;
    @JsonProperty("release_date")
    Date releaseDate;
    @JsonProperty("production_countries")
    List<MovieCountryDTO> countries;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MovieCollectionDTO getCollection() {
        return collection;
    }

    public void setCollection(MovieCollectionDTO collection) {
        this.collection = collection;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<MovieCountryDTO> getCountries() {
        return countries;
    }

    public void setCountries(List<MovieCountryDTO> countries) {
        this.countries = countries;
    }
}
