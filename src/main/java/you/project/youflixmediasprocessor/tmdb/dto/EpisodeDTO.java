package you.project.youflixmediasprocessor.tmdb.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EpisodeDTO extends ResultDTO {

    @JsonProperty("episode_number")
    private Integer episode;

    @JsonProperty("season_number")
    private Integer season;

    private String name;

    private String show;

    public Integer getEpisode() {
        return episode;
    }

    public void setEpisode(Integer episode) {
        this.episode = episode;
    }

    public Integer getSeason() {
        return season;
    }

    public void setSeason(Integer season) {
        this.season = season;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }
}
