package you.project.youflixmediasprocessor.tmdb.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MoviesDTO extends ResultDTO {

    @JsonProperty("results")
    List<MovieDTO> movies;

    public List<MovieDTO> getMovies() {
        return movies;
    }

    public void setMovies(List<MovieDTO> movies) {
        this.movies = movies;
    }

    @JsonIgnore
    public int size(){
        if(movies == null){
            return 0;
        }
        return movies.size();
    }
}
