package you.project.youflixmediasprocessor.tmdb.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import you.project.youflixmediasprocessor.exceptions.MediaProcessingException;
import you.project.youflixmediasprocessor.tmdb.dto.CountDTO;
import you.project.youflixmediasprocessor.tmdb.dto.EpisodeDTO;
import you.project.youflixmediasprocessor.tmdb.dto.ShowsDTO;

import java.util.Map;

@Service
public class SerieRestClient {

    @Autowired
    private ApiService apiService;

    @Value("${api.tmdb.url.serie.get}")
    private String urlGet;
    @Value("${api.tmdb.url.serie.search}")
    private String urlSearch;

    @Value("${api.tmdb.language}")
    private String lang;

    public EpisodeDTO getEpisode(Integer showId, Integer season, Integer episode, String show) throws MediaProcessingException {
        Map<String, String> params = apiService.getDefaultsParams();
        params.put("show_id", showId.toString());
        params.put("season_number", season.toString());
        params.put("episode_number", episode.toString());


        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<EpisodeDTO> response;
        try {
            response = restTemplate.getForEntity(urlGet, EpisodeDTO.class, params);
            apiService.checkRateLimitAndWait(response.getHeaders());
            return response.getBody();
        } catch(HttpClientErrorException.NotFound e){
            MediaProcessingException ex = new MediaProcessingException("Impossible to find episode [" + season + "]x[" + episode + "] for TV show [" + show + "].");
            ex.setOrigException(e);
            throw ex;
        }
    }

    public CountDTO searchCountShow(String query){
        Map<String, String> params = apiService.getDefaultsParams();
        params.put("query", query);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<CountDTO> response = restTemplate.getForEntity(urlSearch, CountDTO.class, params);
        apiService.checkRateLimitAndWait(response.getHeaders());
        return response.getBody();
    }

    public ShowsDTO searchShow(String query){
        Map<String, String> params = apiService.getDefaultsParams();
        params.put("query", query);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ShowsDTO> response = restTemplate.getForEntity(urlSearch, ShowsDTO.class, params);
        apiService.checkRateLimitAndWait(response.getHeaders());
        return response.getBody();
    }
}

