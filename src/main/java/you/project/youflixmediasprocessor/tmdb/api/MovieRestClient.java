package you.project.youflixmediasprocessor.tmdb.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import you.project.youflixmediasprocessor.tmdb.dto.CountDTO;
import you.project.youflixmediasprocessor.tmdb.dto.MovieDTO;
import you.project.youflixmediasprocessor.tmdb.dto.MoviesDTO;

import java.util.Map;

@Service
public class MovieRestClient {

    @Autowired
    private ApiService apiService;

    @Value("${api.tmdb.url.movie.get}")
    private String urlGet;

    @Value("${api.tmdb.url.movie.search}")
    private String urlSearch;

    public MovieDTO getMovieById(Integer id){
        Map<String, String> params = apiService.getDefaultsParams();
        params.put("movie_id", id.toString());

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<MovieDTO> response = restTemplate.getForEntity(urlGet, MovieDTO.class, params);
        apiService.checkRateLimitAndWait(response.getHeaders());
        return response.getBody();

    }

    public CountDTO searchCountMovie(String query, String year){
        Map<String, String> params = apiService.getDefaultsParams();
        params.put("query", query);
        params.put("year", year);


        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<CountDTO> response = restTemplate.getForEntity(urlSearch, CountDTO.class, params);
        apiService.checkRateLimitAndWait(response.getHeaders());
        return response.getBody();
    }

    public MoviesDTO searchMovie(String query, String year){
        Map<String, String> params = apiService.getDefaultsParams();
        params.put("query", query);
        params.put("year", year);
        
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<MoviesDTO> response = restTemplate.getForEntity(urlSearch, MoviesDTO.class, params);
        apiService.checkRateLimitAndWait(response.getHeaders());
        return response.getBody();
    }
}
