package you.project.youflixmediasprocessor.tmdb.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class ApiService {

    @Value("${api.tmdb.interval}")
    String interval;
    @Value("${api.tmdb.language}")
    private String lang;
    @Value("${api.tmdb.key}")
    private String apiKey;

    private static final Logger logger = LoggerFactory.getLogger(ApiService.class);


    /**
     * Calculate a waiting time between call to API in order to not reach the rate limit
     *
     * @param headers
     */
    public void checkRateLimitAndWait(HttpHeaders headers) {
        List<String> remaining = headers.get("X-RateLimit-Remaining");
        List<String> reset = headers.get("X-RateLimit-Reset");
        List<String> date = headers.get("Date");

        //Default interval between API calls
        long wait = Long.valueOf(interval);

        if(remaining != null && remaining.size() == 1
                && reset != null && reset.size() == 1
                && date != null && date.size() == 1 ) {
            try {
                long remainingCalls = Long.valueOf(remaining.get(0));
                long resetTime = Long.valueOf(reset.get(0)) * 1000L;
                long millisUntilReset = resetTime - this.getServerTime(date.get(0));
                wait = (millisUntilReset / remainingCalls) + 1;
            } catch (ParseException e) {
                logger.error("Impossible to parse server time [{}] into a timestamp", date.get(0));
            }
        }

        try {
            TimeUnit.MILLISECONDS.sleep(wait);
        } catch (InterruptedException e) {
            logger.warn("Rate limit waiting thread interrupted.", e);
        }
    }

    private long getServerTime(String date) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH);
        Date serverDate = simpleDateFormat.parse(date);
        return serverDate.getTime();
    }


    public Map<String, String> getDefaultsParams(){
        Map<String, String> params = new HashMap<>();
        params.put("api_key", apiKey);
        params.put("lang", lang);
        return params;
    }



}
