package you.project.youflixmediasprocessor.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import you.project.youflixmediasprocessor.exceptions.MediaProcessingException;
import you.project.youflixmediasprocessor.media.Media;
import you.project.youflixmediasprocessor.tmdb.api.MovieRestClient;
import you.project.youflixmediasprocessor.tmdb.dto.CountDTO;
import you.project.youflixmediasprocessor.tmdb.dto.MovieCountryDTO;
import you.project.youflixmediasprocessor.tmdb.dto.MovieDTO;
import you.project.youflixmediasprocessor.tmdb.dto.MoviesDTO;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MovieService {

    @Autowired
    private TokenService tokenService;
    @Autowired
    private MovieRestClient apiClient;
    @Autowired
    private FileService fileService;

    @Value("${media.movie.name}")
    private String namingPattern;

    public void process(Media media) throws MediaProcessingException {

        MovieDTO movie = this.getMovie(media.getOrigName());
        MovieDTO detailedMovie = apiClient.getMovieById(movie.getId());

        media.setProcName(this.generateFileName(detailedMovie));
        fileService.writeToOutput(media);
    }

    private String generateFileName(MovieDTO movie) {

        String name = namingPattern;

        name = fileService.processNaming(name, "{title}", movie.getTitle());

        if(movie.getCollection() != null){
            name = fileService.processNaming(name, "{collection}", movie.getCollection().getName());
        }else{
            name = fileService.processNaming(name, "{collection}", null);
        }

        List<String> countryList = new ArrayList<>();
        if(movie.getCountries() != null && !movie.getCountries().isEmpty()){
            for(MovieCountryDTO country : movie.getCountries()){
                countryList.add(country.getName());
            }
        }

        name = fileService.processNaming(name, "{countries}", String.join(",", countryList));

        String year = null;
        if(movie.getReleaseDate() != null){
            year = getDateString(movie.getReleaseDate());
        }

        name = fileService.processNaming(name, "{year}", year);

        return name;

    }

    private String getDateString(Date date) {
        String dateStr;
        SimpleDateFormat df = new SimpleDateFormat("yyyy");
        dateStr = df.format(date);
        return dateStr;
    }

    private MovieDTO getMovie(String movieToken) throws MediaProcessingException {

        // Try to find narrowest result set by searching filename tokens incrementally
        List<String> possibleYears = new ArrayList<>(tokenService.getYearsInFilename(movieToken));
        possibleYears.add("");
        List<MoviesDTO> resultsSets = new ArrayList<>();
        MoviesDTO results;
        for(String year : possibleYears){
            results = findMoviesByIncrementalSearch(movieToken, year);
            // One result found, that must be it
            if(results.size() == 1){
                return results.getMovies().get(0);
            }
            if(results.size() > 0){
                resultsSets.add(results);
            }
        }

        StringBuilder error = new StringBuilder("Can't find a matching movie ID for token [" + movieToken + "]\n");

        if(resultsSets.isEmpty()) {
            error.append("No result found.");
            throw new MediaProcessingException(error.toString());
        }

        // Impossible to find a solid match, abort for this media
        error.append("Narrowest results sets were :\n");
        for(MoviesDTO resultsSet : resultsSets){
            error.append(resultsSet).append("\n");
        }
        throw new MediaProcessingException(error.toString());
    }

    private MoviesDTO findMoviesByIncrementalSearch(String movieToken, String year){
        List<String> tokens = tokenService.tokenize(movieToken);
        String search = "";
        for(String token : tokens){
            String newSearch = search + "+" + token;
            CountDTO count = apiClient.searchCountMovie(newSearch, year);
            if(count.getResults() == 1){
                // Only one result, that must be it
                return apiClient.searchMovie(newSearch, year);
            }else if(count.getResults() > 1){
                // There is still results,
                // so the current token will be included in the next search
                search = newSearch;
            }
        }
        return apiClient.searchMovie(search, year);
    }
}
