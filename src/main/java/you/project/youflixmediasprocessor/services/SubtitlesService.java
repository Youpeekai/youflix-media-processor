package you.project.youflixmediasprocessor.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import you.project.youflixmediasprocessor.media.SubtitlesMedia;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

@Service
public class SubtitlesService {

    @Autowired
    private TokenService tokenService;

    public void setLocale(SubtitlesMedia media){
        Locale[] locales = SimpleDateFormat.getAvailableLocales();
        List<String> tokens = tokenService.tokenize(media.getOrigName());
        for(Locale loc : locales){
            if(tokens.contains(loc.getISO3Language()) ||
                    tokens.contains(loc.getDisplayLanguage(Locale.ENGLISH)) ||
                    tokens.contains(loc.getLanguage())){
                media.setLanguage(loc.getLanguage());
                return;
            }
        }
    }

}
