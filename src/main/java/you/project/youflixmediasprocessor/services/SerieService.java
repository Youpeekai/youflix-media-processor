package you.project.youflixmediasprocessor.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import you.project.youflixmediasprocessor.exceptions.MediaProcessingException;
import you.project.youflixmediasprocessor.media.Media;
import you.project.youflixmediasprocessor.tmdb.api.SerieRestClient;
import you.project.youflixmediasprocessor.tmdb.dto.CountDTO;
import you.project.youflixmediasprocessor.tmdb.dto.EpisodeDTO;
import you.project.youflixmediasprocessor.tmdb.dto.ShowDTO;
import you.project.youflixmediasprocessor.tmdb.dto.ShowsDTO;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class SerieService {

    @Autowired
    private SerieRestClient apiClient;
    @Autowired
    private FileService fileService;
    @Autowired
    private TokenService tokenService;

    @Value("${media.serie.name}")
    private String namingPattern;

    @Value("${media.serie.separators}")
    private String separators;

    public void process(Media media) throws MediaProcessingException {
        String numberingToken = this.getNumberingToken(media.getOrigName());
        String[] numbers = numberingToken.split(tokenService.getSeparatorsRegex());
        int seasonNumber = this.getSeasonNumber(numbers[0]);
        int episodeNumber = this.getEpisodeNumber(numbers[1]);

        String showToken = this.getShowToken(media.getOrigName(), media.getOrigName().indexOf(numberingToken));

        EpisodeDTO episode;
        ShowDTO show = this.getShow(showToken);
        episode = this.getEpisode(show, seasonNumber, episodeNumber);
        episode.setShow(show.getName());

        media.setProcName(this.generateFileName(episode));

        fileService.writeToOutput(media);
    }

    private EpisodeDTO getEpisode(ShowDTO show, int seasonNumber, int episodeNumber) throws MediaProcessingException {
        EpisodeDTO episode = apiClient.getEpisode(show.getId(), seasonNumber, episodeNumber, show.getName());
        if(episode == null){
            throw new MediaProcessingException("No episode found on for show [" + show.getId() + "][" + show.getName() + "] " +
                    "and season number [" + seasonNumber + "] " +
                    "and episode number [" + episodeNumber + "]");
        }
        return episode;
    }

    private int getSeasonNumber(String token){
        return Integer.parseInt(token);
    }

    private int getEpisodeNumber(String episodeToken){
        return Integer.parseInt(episodeToken);
    }

    private ShowDTO getShow(String showToken) throws MediaProcessingException {
        ShowsDTO results = this.findShowsByIncrementalSearch(showToken);

        // One result found, that must be it
        if(results.size() == 1){
            return results.getShows().get(0);
        }

        String error = "Can't find a matching show ID for token [" + showToken + "]\n";

        if(results.size() == 0) {
            error += "No result found.";
            throw new MediaProcessingException(error);
        }

        // Try to find one movie with the minimal title words count
        ShowDTO show = this.findShowByWordCount(results);

        if(show != null){
            return show;
        }

        // Impossible to find a solid match, abort for this media
        error += "Narrowest results set was :\n" + results;
        throw new MediaProcessingException(error);
    }

    private ShowDTO findShowByWordCount(ShowsDTO results) {
        int min = 0;
        ShowDTO result = null;
        for(ShowDTO show : results.getShows()){
            List<String> name = tokenService.tokenize(show.getName());
            if(min == 0 || (name.size() > 0 && name.size() < min)){
                min = name.size();
                result = show;
            }else if (min == name.size()){
                // At least two shows have the same word count
                return null;
            }
        }
        return result;

    }

    private ShowsDTO findShowsByIncrementalSearch(String showToken){
        List<String> tokens = tokenService.tokenize(showToken);
        String search = "";
        for(String token : tokens){
            String newSearch = search + "+" + token;

            CountDTO count = apiClient.searchCountShow(newSearch);
            if(count.getResults() == 1){
                // Only one result, that must be it
                return apiClient.searchShow(newSearch);
            }else if(count.getResults() > 1){
                // There is still results,
                // so the current token will be included in the next search
                search = newSearch;
            }
        }
        return apiClient.searchShow(search);
    }

    private String getNumberingToken(String fileName) throws MediaProcessingException {
        Pattern pattern = Pattern.compile(tokenService.getNumberingTokenRegex());
        Matcher matcher = pattern.matcher(fileName);
        if (matcher.find() && !matcher.group().isEmpty())
        {
            return matcher.group();
        }
        throw new MediaProcessingException("No episode numbering token found in filename [" + fileName + "]");
    }

    private String getShowToken(String fileName, int endIndex){
        return fileName.substring(0, endIndex);
    }

    private String generateFileName(EpisodeDTO episode) {
        String name = namingPattern;

        name = fileService.processNaming(name, "{show}", episode.getShow());

        name = fileService.processNaming(name, "{season}", episode.getSeason().toString());

        name = fileService.processNaming(name, "{episode}", String.format("%02d",episode.getEpisode()));

        name = fileService.processNaming(name, "{title}", episode.getName());

        return name;

    }
}
