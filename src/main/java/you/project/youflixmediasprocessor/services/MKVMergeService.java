package you.project.youflixmediasprocessor.services;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import you.project.youflixmediasprocessor.media.Media;
import you.project.youflixmediasprocessor.media.SubtitlesMedia;
import you.project.youflixmediasprocessor.media.VideoMedia;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MKVMergeService {

    private static final Logger logger = LoggerFactory.getLogger(MKVMergeService.class);

    @Value("${path.output}")
    private String output;
    @Value("${path.mkvmerge")
    private String mkvMerge;

    public void merge(List<Media> processedMedias) throws IOException {

        if(mkvMerge == null || mkvMerge.isEmpty()){
            return;
        }

        if(!new File(mkvMerge).exists()){
            logger.error("[{}] does not exists. Cant't merge files into MKV.", mkvMerge);
            return;
        }

        if(!new File(mkvMerge).canExecute()){
            logger.error("[{}] is not executable. Cant't merge files into MKV.", mkvMerge);
            return;
        }

        List<Media> videos = processedMedias.stream()
                .filter(media -> media instanceof VideoMedia)
                .collect(Collectors.toList());
        List<Media> subtitles = processedMedias.stream()
                .filter(media -> media instanceof SubtitlesMedia)
                .collect(Collectors.toList());
        for(Media video : videos){
            String name = FilenameUtils.removeExtension(video.getProcName());
            List<Media> videoSubtitles = new ArrayList<>();
            for(Media subtitle : subtitles){
                if(subtitle.getProcName().startsWith(name)){
                    videoSubtitles.add(subtitle);
                }
            }
            this.mkvMerge(video.getProcName(), videoSubtitles);
        }

    }

    private void mkvMerge(String videoProcName, List<Media> videoSubtitles) throws IOException {

        List<String> command = new ArrayList<>();
        command.add(mkvMerge);

        command.add("--output");
        command.add(output + "/" + FilenameUtils.removeExtension(videoProcName) + ".tmp");

        command.add(output + "/" + videoProcName);

        int count = 0;
        for(Media subtitle : videoSubtitles){
            command.add("--sub-charset");
            command.add("0:UTF-8");
            command.add("--language");
            command.add(count + ":" + ((SubtitlesMedia) subtitle).getLanguage());
            command.add(output + "/" + subtitle.getProcName());
        }

        ProcessBuilder builder = new ProcessBuilder(command);
        builder.redirectErrorStream(true);
        Process process = builder.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
    }
}
