package you.project.youflixmediasprocessor.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TokenService {

    @Value("${media.serie.separators}")
    private String[] separators;

    public List<String> tokenize(String input){
        List<String> tokens = new LinkedList<>(Arrays.asList(input.split("\\W")));

        tokens.removeAll(Arrays.asList("", null));
        return tokens;
    }

    public String getSeparatorsRegex(){
        return String.join("|", separators);
    }

    public String getNumberingTokenRegex(){
        return "\\d+(" + getSeparatorsRegex() + ")\\d+";
    }

    public List<String> getYearsInFilename(String filename){
        List<String> results = new ArrayList<>();
        Pattern pattern = Pattern.compile("[1-2]\\d{3}");
        Matcher matcher = pattern.matcher(filename);
        while(matcher.find()){
            results.add(matcher.group());
        }
        return results;
    }


}
