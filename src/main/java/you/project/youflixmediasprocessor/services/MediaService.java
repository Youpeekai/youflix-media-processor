package you.project.youflixmediasprocessor.services;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import you.project.youflixmediasprocessor.exceptions.GlobalProcessingException;
import you.project.youflixmediasprocessor.exceptions.MediaProcessingException;
import you.project.youflixmediasprocessor.media.Media;
import you.project.youflixmediasprocessor.media.SubtitlesMedia;
import you.project.youflixmediasprocessor.media.VideoMedia;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class MediaService {

    private static final Logger logger = LoggerFactory.getLogger(MediaService.class);

    @Value("${path.input}")
    private String inputDir;
    @Value("${srt.extensions}")
    private String[] srtExtensions;
    @Value("${media.extensions}")
    private String[] mediaExtensions;
    @Value("${path.output}")
    private String outputDir;
    @Value("${path.error}")
    private String errorDir;
    @Value("${api.tmdb.key}")
    private String apiKey;


    @Autowired
    private SerieService serieService;
    @Autowired
    private MovieService movieService;
    @Autowired
    private FileService fileService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private SubtitlesService subtitlesService;

    public void process(){
        try {
            this.processInputDirectory();
        } catch (GlobalProcessingException e) {
            logger.error("Fatal error occured. Processing will be interrupted\n{}", e.getLocalizedMessage());
        } catch (Exception ex){
            logger.error("Fatal error occured. Processing will be interrupted.", ex);
        }
    }

    private void processInputDirectory() throws GlobalProcessingException {

        this.checkParamaters();

        File input =  new File(inputDir);

        List<File> filesToPurge = new ArrayList<>();
        if(input.listFiles() != null){
            filesToPurge = List.of(input.listFiles());
        }
        List<File> filesToIgnore = new ArrayList<>();
        List<Media> processedMedias = new ArrayList<>();

        for(File file : this.getManagedFiles(input)){

            if(!fileService.isFileToIgnore(file)){
                processedMedias.add(this.processFile(file));
            }else{
                filesToIgnore.add(file);
            }
        }
        this.purge(filesToPurge, filesToIgnore);
    }

    private Collection<File> getManagedFiles(File input){
        String[] extensions = fileService.getAllExtensions();
        return FileUtils.listFiles(input, extensions, true);
    }

    private void handleMediaProcessingException(MediaProcessingException ex) throws GlobalProcessingException {
        fileService.writeToError(ex);
        logger.error("An error occured while processing a media file. This file won't be processed.\n{}", ex.getLocalizedMessage());
    }

    /**
     * Delete a list of files & directories
     *
     * @param filesToPurge
     */
    private void purge(List<File> filesToPurge, List<File> filesToIgnore) {
        for(File toPurge : filesToPurge){
            boolean ignore = false;
            for(File toIgnore : filesToIgnore){
                if(toPurge.getAbsolutePath().equals(toIgnore.getAbsolutePath())){
                    ignore = true;
                }
            }
            if(!ignore){
                FileSystemUtils.deleteRecursively(toPurge);
            }
        }
    }

    /**
     * Process a file as a media
     *
     * @param file
     */
    private Media processFile(File file) throws GlobalProcessingException {

        Media media = this.getMediaFromFile(file);

        try {
            this.processMedia(media);
        } catch (MediaProcessingException ex){
            ex.setMedia(media);
            this.handleMediaProcessingException(ex);
        }
        return media;
    }

    /**
     * Process an identified media
     *
     * @param media
     * @throws MediaProcessingException
     */
    private void processMedia(Media media) throws MediaProcessingException {
        media.setMediaType(getMediaType(media.getOrigName()));

        if(media instanceof SubtitlesMedia){
            subtitlesService.setLocale((SubtitlesMedia) media);
        }

        if(media.getMediaType() == Media.MediaTypeEnum.SERIE){
            serieService.process(media);
        }else{
            movieService.process(media);
        }
    }

    /**
     * Identifies the type of media, SERIE or MOVIE
     *
     * @param origName
     * @return
     */
    private Media.MediaTypeEnum getMediaType(String origName) {
        if(origName.matches(".*" + tokenService.getNumberingTokenRegex() + ".*")){
            return Media.MediaTypeEnum.SERIE;
        }
        return Media.MediaTypeEnum.MOVIE;
    }

    private Media getMediaFromFile(File file){
        String ext = FilenameUtils.getExtension(file.getName());
        Media media;
        if(List.of(srtExtensions).contains(ext)){
            media = new SubtitlesMedia();
        }else{
            media = new VideoMedia();
        }
        media.setFile(file);
        media.setOrigName(file.getName());
        return media;
    }

    private void checkParamaters() throws GlobalProcessingException {
        String msg = null;
        if(!fileService.isFileWritable(inputDir)){
            msg = "Input directory [" + inputDir + "] does not exists or is not readable/writable";
        }
        if(!fileService.isOutputWritable()){
            msg = "Output directory [" + outputDir + "] does not exists or is not readable/writable";
        }
        if(!fileService.isErrorOutputWritable()){
            msg = "Error directory [" + errorDir + "] does not exists or is not readable/writable";
        }
        if(apiKey == null || apiKey.isEmpty()){
            msg = "API key is not set for TheMoviesDataBase. File renaming is impossible.";
        }

        if(msg != null){
            throw new GlobalProcessingException(msg);
        }
    }
}
