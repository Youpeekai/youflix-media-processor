package you.project.youflixmediasprocessor.services;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import you.project.youflixmediasprocessor.exceptions.GlobalProcessingException;
import you.project.youflixmediasprocessor.exceptions.MediaProcessingException;
import you.project.youflixmediasprocessor.media.Media;
import you.project.youflixmediasprocessor.media.SubtitlesMedia;
import you.project.youflixmediasprocessor.media.VideoMedia;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileService {

    private static final Logger logger = LoggerFactory.getLogger(FileService.class);

    public static final String UNKNOWN = "###";

    @Value("${srt.extensions}")
    private String[] srtExtensions;

    @Value("${media.extensions}")
    private String[] mediaExtensions;

    @Value("${path.output}")
    private String output;

    @Value("${path.error}")
    private String error;

    @Value("${path.input}")
    private String input;


    /**
     * Write the provided media to the output directory
     *
     * @param media
     * @throws IOException
     * @throws MediaProcessingException
     */
    public void writeToOutput(Media media) throws MediaProcessingException {

        File result = this.getProcessedFile(media);

        try {
            media.getFile().mkdirs();
            FileUtils.copyFile(media.getFile(), result);
        } catch (Exception e) {
            MediaProcessingException ex = new MediaProcessingException("File [" + media.getFile().getAbsolutePath() + "] couldn't be created following an unexpected error.");
            ex.setOrigException(e);
            throw ex;
        }
        this.writeToRegister(media);
    }

    private File getProcessedFile(Media media) throws MediaProcessingException {
        String ext = "." + FilenameUtils.getExtension(media.getOrigName());
        if (media instanceof SubtitlesMedia){
            ext = "." + ((SubtitlesMedia) media).getLanguage() + ext;
        }
        String name = this.cleanFilename(media.getProcName());
        Path path = Paths.get(output + "/" + name + ext);

        if(!Files.exists(path)){
            return path.toFile();
        }

        // if a video file already exists on this path, it won't be overwritten
        if(media instanceof VideoMedia){
            throw new MediaProcessingException("File [" + path + "] alreday exists and won't be overwritten.");
        }

        // if a subtitles file already exists on this path
        // the processed file will be numbered (e.g. "Metropolis (1927).2.en.sub")
        // there is no such things as "too many choices of subtitles"
        int count = 0;
        while(Files.exists(path)){
            count++;
            path = Paths.get(output + "/" + name + "." + count + ext);
        }
        return path.toFile();
    }

    /**
     * Remove illegal filename characters
     * and replace double or more spaces with a single one
     *
     * @param procName
     * @return
     */
    private String cleanFilename(String procName) {
        // remove illegal characters
        String safe = procName.replaceAll(
                "[\\*|\\:|\\||\"|\\<|\\>|\\{|\\}|\\?|\\%]",
                "");
        // replace double or more spaces with a single one
        safe = safe.replaceAll("_{2,}", "_");
        return safe;
    }

    /**
     * Write an error log message and the unprocessed media to the error output directory
     * in a timestamped subdirectory
     *
     * @param ex
     * @throws IOException
     */
    public void writeToError(MediaProcessingException ex) throws GlobalProcessingException {
        String path = error + "/"
                + ex.getMedia().getOrigName() + "_"
                + ex.getMedia().getTimestamp().getTime()
                + "/" + ex.getMedia().getOrigName();
        List<String> content = new ArrayList<>();
        content.add(ex.getLocalizedMessage());

        try {
            File logFile = new File(path + ".log");
            logFile.getParentFile().mkdirs();
            Files.write(logFile.toPath(), content, StandardCharsets.UTF_8);
            logger.info("Error log file : [{}]", path + ".log");
        } catch (IOException e) {
            GlobalProcessingException gex = new GlobalProcessingException("An input/output error occured while an error log file was being written to [" + path + ".log]");
            gex.setOrigException(e);
            throw gex;
        }
        try {
            FileUtils.copyFile(ex.getMedia().getFile(), new File(path));
            logger.info("Rejected media file : [{}]", path);
        } catch (IOException e) {
            GlobalProcessingException gex = new GlobalProcessingException("An input/output error occured while a file in error was being written to [" + path + "]");
            gex.setOrigException(e);
            throw gex;
        }
    }

    public void writeToRegister(Media media){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String date = LocalDateTime.now().format(formatter);

        String log = "File [" + input + "/" + media.getOrigName() + "] moved to [" + output + "/" + media.getProcName() + "]";
        logger.info(log);
        log = date + " " + log;
        String path = output + "/youflix-media-processor_register.log";
        File register = new File(path);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(register, true))) {
            writer.write(log);
            writer.newLine();
        } catch (IOException e){
             logger.error("File processing couldn't be logged to register file [{}] due to input/output error.", path);
        }
    }

    /**
     * Replace a property (e.g. '{title}') by its provided value in the provided filename
     *
     * If the provided value is null or empty, will replace the property by :
     * - nothing if it's the name of a subdirectory (i.e. it will strip this directory from the path)
     * - the value of the application property 'media.properties.unknow' otherwise
     *
     * @param name
     * @param property
     * @param value
     */
    public String processNaming(String name, String property, String value){
        if (value != null && !value.isEmpty()) {
            return StringUtils.replace(name, property, value);
        }
        //if the property is used as a directory name, strip it from the path
        name = StringUtils.replace(name, "/" + property + "/", "/");
        name = StringUtils.replace(name, "^" + property + "/", "");

        name = StringUtils.replace(name, property, UNKNOWN);

        return name;
    }

    /**
     * provide an array containing all managed extension (video & subtitle)
     *
     * @return
     */
    public String[] getAllExtensions(){
        return StringUtils.concatenateStringArrays(srtExtensions, mediaExtensions);
    }

    public boolean isOutputWritable(){
        return this.isFileWritable(output);
    }

    public boolean isErrorOutputWritable() {
        return this.isFileWritable(error);
    }

    /**
     * Check existence, readabilty and writability of a given path
     *
     * @param path
     * @return
     */
    public boolean isFileWritable(String path){
        File file = new File(path);

        if(!file.exists()){
            logger.error("[{}] doesn't exists.", path);
            return false;
        }

        if(!file.canRead()){
            logger.error("[{}] is not readable", path);
            return false;
        }

        if(!file.canWrite()){
            logger.error("[{}] is not writable", path);
            return false;
        }
        return true;
    }

    public boolean isFileToIgnore(File file){

        if(!this.isFileWritable(file.getAbsolutePath())){
            logger.warn("File [{}] is not writable, and therefore can't be processed. It will be ignored but won't be deleted from input directory.", file.getAbsolutePath());
            return true;
        }

        RandomAccessFile stream = null;
        try {
            stream = new RandomAccessFile(file, "rw");
            return false;
        } catch (FileNotFoundException e) {
            logger.warn("File [{}] can't be processed (probably still being written). " +
                    "It will be ignored but won't be deleted from input directory.", file.getAbsolutePath(), e);
            return true;
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    logger.error("Exception during closing file " + file.getName());
                }
            }
        }
    }

}
