package you.project.youflixmediasprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import you.project.youflixmediasprocessor.services.MediaService;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
		MediaService service = context.getBean(MediaService.class);

		service.process();

		context.close();
	}

}
