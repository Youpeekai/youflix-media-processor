package you.project.youflixmediasprocessor.media;

public class SubtitlesMedia extends Media {

    private String language = "";

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString(){
        return super.toString()
                + "Language : [" + language + "]";
    }
}
