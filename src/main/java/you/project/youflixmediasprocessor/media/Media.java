package you.project.youflixmediasprocessor.media;

import java.io.File;
import java.sql.Timestamp;

public abstract class Media {

    private Timestamp timestamp =  new Timestamp(System.currentTimeMillis());

    private File file;
    private MediaTypeEnum mediaType;
    private String origName;
    private String procName;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getOrigName() {
        return origName;
    }

    public void setOrigName(String origName) {
        this.origName = origName;
    }

    public String getProcName() {
        return procName;
    }

    public void setProcName(String procName) {
        this.procName = procName;
    }

    public MediaTypeEnum getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaTypeEnum mediaType) {
        this.mediaType = mediaType;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString(){
        return "File path : [" + file.getAbsolutePath() + "]\n"
                + "Timestamp : [" + timestamp + "]\n"
                + "Type : [" + mediaType + "]\n"
                + "Original Name : [" + origName + "]\n"
                + "Processed Name : [" + procName + "]";
    }

    public enum MediaTypeEnum {
        MOVIE, SERIE
    }

}
