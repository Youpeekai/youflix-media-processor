package you.project.youflixmediasprocessor.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Exception to throw if the global processing must be interrupted
 *
 */
public class GlobalProcessingException extends Exception {

    private String userMessage;
    private Exception origException;

    public GlobalProcessingException(String userMessage){
        super();
        this.userMessage = userMessage;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public Exception getOrigException() {
        return origException;
    }

    public void setOrigException(Exception origException) {
        this.origException = origException;
    }

    @Override
    public String getLocalizedMessage(){
        String msg = "====== CAUSE ======\n" + userMessage;
        if(origException != null){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            origException.printStackTrace(pw);
            msg += "\n====== TECHNICAL MESSAGE ======\n"
                    + sw.toString();
        }
        return msg;
    }


}
