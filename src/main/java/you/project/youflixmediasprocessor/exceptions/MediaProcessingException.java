package you.project.youflixmediasprocessor.exceptions;

import you.project.youflixmediasprocessor.media.Media;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Exception to throw if the processing of one media must be interrupted
 *
 */
public class MediaProcessingException extends Exception {

    private String userMessage;
    private Media media;
    private Exception origException;

    public MediaProcessingException(String userMessage){
        super();
        this.userMessage = userMessage;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public Exception getOrigException() {
        return origException;
    }

    public void setOrigException(Exception origException) {
        this.origException = origException;
    }

    @Override
    public String getLocalizedMessage(){
        String msg = "====== CAUSE ======\n" + userMessage;
        if(media != null){
            msg += "\n====== MEDIA INFOS ======\n"
                    + media.toString();
        }
        if(origException != null){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            origException.printStackTrace(pw);
            msg += "\n====== TECHNICAL MESSAGE ======\n"
                    + sw.toString();
        }
        return msg;
    }


}
